import com.twuc.webApp.Bag;
import com.twuc.webApp.SizeType;
import com.twuc.webApp.Storage;
import com.twuc.webApp.Ticket;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ApplicationTest {
    @Test
    void shouldGetTheTicketWhenSavingABag() {
        Storage storage = new Storage(2L);
        Ticket ticket = storage.save(new Bag());
        assertNotNull(ticket);
    }

    @Test
    @SuppressWarnings("ConstantConditions")
    void should_get_ticket_when_giving_nothing() {
        Storage storage = new Storage(2L);
        final Bag nothing = null;
        Ticket ticket = storage.save(nothing);
        assertNotNull(ticket);
    }

    @Test
    void should_get_bag_when_retrieve_vaild_ticket() throws Exception {
        Storage storage = new Storage(2L);
        Bag savedBag = new Bag();
        Ticket ticket = storage.save(savedBag);
        Bag retrievedBag = storage.retrieve(ticket);

        assertSame(retrievedBag, savedBag);
    }

    @Test
    void should_return_the_bags_when_save_the_bag_retrieve_the_ticket() throws Exception {
        Storage storage = new Storage(2L);
        Bag savedBag = new Bag();
        Ticket ticket = storage.save(savedBag);

        Bag savedAnotherBag = new Bag();
        Ticket anotherTicket = storage.save(savedAnotherBag);

        Bag retrievedBag = storage.retrieve(ticket);
        assertSame(retrievedBag, savedBag);

    }

    @Test
    void should_return_error_message_when_given_a_invalid_ticket() {
        Storage storage = new Storage(2L);
        Bag savedBag = new Bag();
        storage.save(savedBag);

        Ticket invalidTicket = new Ticket();

        IllegalArgumentException theException = assertThrows(
                IllegalArgumentException.class,
                () -> storage.retrieve(invalidTicket)
        );
        assertEquals(theException.getMessage(), "Invalid Ticket");
    }

    @Test
    @SuppressWarnings("ConstantConditions")
    void should_return_nothing_when_retrieve_bag_and_if_save_nothing() throws Exception {
        Storage storage = new Storage(2L);
        final Bag nothing = null;
        Ticket validTicket = storage.save(nothing);

        Bag retrieveNothing = storage.retrieve(validTicket);
        assertNull(retrieveNothing);
    }

    @Test
    void should_get_a_ticket_when_save_the_bag_in_empty_storage_with_2() {
        Storage emptryStorage = new Storage(2L);
        Bag savedBag = new Bag();
        Ticket ticket = emptryStorage.save(savedBag);
        assertNotNull(ticket);
    }

    @Test
    void should_get_message_when_save_the_bag_with_full_storage() {
        Storage storage = new Storage(2L);
        Bag savedBag = new Bag();
        Bag savedBagAnother = new Bag();
        storage.save(savedBag);
        storage.save(savedBagAnother);

        Bag unsavedBage = new Bag();
        IllegalArgumentException theException = assertThrows(
                IllegalArgumentException.class,
                () -> storage.save(unsavedBage)
        );
        assertEquals(theException.getMessage(), "Insufficient capacity!");
    }

    @Test
    void should_return_a_ticket_and_message_when_save_2_bags_with_storage_1_slot() {
        Storage storage = new Storage(1L);
        Bag savedBag = new Bag();
        Ticket ticket = storage.save(savedBag);
        assertNotNull(ticket);

        Bag unsavedBag = new Bag();
        assertThrows(
                IllegalArgumentException.class,
                () -> storage.save(unsavedBag),
                "Insufficient capacity!"
        );
    }

    //  size bag

    @Test
    void should_return_a_vaild_ticket_when_save_a_small_bag_with_a_big_slot_storage() throws IllegalAccessException {
        Storage storage = new Storage(1, 0, 0);
        Bag smallBag = new Bag(SizeType.SMALL);
        SizeType bigSlot = SizeType.BIG;
        Ticket ticket = storage.save(smallBag, bigSlot);
        assertNotNull(ticket);
    }

    @Test
    void should_return_error_message_when_save_big_bag_to_small_slot() {
        Storage storage = new Storage(2, 2, 2);
        Bag bigBag = new Bag(SizeType.BIG);
        SizeType smallSlot = SizeType.SMALL;
        IllegalArgumentException theException = assertThrows(
                IllegalArgumentException.class,
                () -> storage.save(bigBag, smallSlot)
        );
        assertEquals(theException.getMessage(), "Cannot save your bag: BIG SMALL");
    }

    @Test
    void should_return_error_message_when_save_a_big_bag_to_0_big_slot() {
        Storage storage = new Storage(0, 1, 1);
        Bag bigBag = new Bag(SizeType.BIG);
        SizeType bigSlot = SizeType.BIG;
        IllegalArgumentException theException = assertThrows(
                IllegalArgumentException.class,
                () -> storage.save(bigBag, bigSlot)
        );
        assertEquals(theException.getMessage(), "Insufficient capacity!");
    }

    @Test
    void should_return_a_valid_ticket_when_save_a_medium_bag_to_1_medium_slot() {
        Storage storage = new Storage(0, 1, 0);
        Bag middleBag = new Bag(SizeType.MIDDLE);
        SizeType middleSlot = SizeType.MIDDLE;
        Ticket ticket = storage.save(middleBag, middleSlot);
        assertNotNull(ticket);
    }
}
