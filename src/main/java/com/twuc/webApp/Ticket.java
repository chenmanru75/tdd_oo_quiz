package com.twuc.webApp;

public class Ticket {
    private SizeType sizeType = SizeType.SMALL;

    public Ticket() {
    }

    public Ticket(SizeType sizeType) {
        this.sizeType = sizeType;
    }

    public SizeType getSizeType() {
        return sizeType;
    }
}
