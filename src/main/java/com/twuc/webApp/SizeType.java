package com.twuc.webApp;

public enum SizeType {
    BIG(3),
    SMALL(2),
    MIDDLE(1);

    private int size;

    SizeType(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }
}
