package com.twuc.webApp;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Storage {
    private Long capacity;
    private Map<Ticket, Bag> bags = new HashMap<>();

    private int bigSlot;
    private int middleSlot;
    private int smallSlot;

    private int usedbigSlot;
    private int usedmiddleSlot;
    private int usedsmallSlot;



    public Storage(Long capacity) {
        this.capacity = capacity;
    }

    public Storage(int bigSlot, int middleSlot, int smallSlot) {
        this.bigSlot = bigSlot;
        this.middleSlot = middleSlot;
        this.smallSlot = smallSlot;
    }

    public Ticket save(Bag bag) {
        if (bags.size() == capacity) {
            throw new IllegalArgumentException("Insufficient capacity!");
        }
        Ticket ticket = new Ticket();
        bags.put(ticket, bag);
        smallSlot--;
        return ticket;
    }

    public Ticket save(Bag bag, SizeType size) throws IllegalArgumentException {
        if(bag.getSize().getSize() > size.getSize()){
            throw new IllegalArgumentException("Cannot save your bag: "+ bag.getSize() +" " + size);
        }

        switch (size){
            case BIG:
                if(this.bigSlot < 1){
                    throw new IllegalArgumentException("Insufficient capacity!");
                }
                this.bigSlot--;
                break;
            case MIDDLE:
                if(this.middleSlot < 1){
                    throw new IllegalArgumentException("Insufficient capacity!");
                }
                this.middleSlot--;
                break;
            case SMALL:
                if(this.smallSlot < 1){
                    throw new IllegalArgumentException("Insufficient capacity!");
                }
                this.smallSlot--;
                break;
            default:throw new IllegalArgumentException("Type error!");
        }
        Ticket ticket = new Ticket(size);
        bags.put(ticket,bag);
        return ticket;
    }

    public Bag retrieve(Ticket ticket) throws Exception {
        if (!bags.containsKey(ticket)) {
            throw new IllegalArgumentException("Invalid Ticket");
        }
        Bag bag = bags.get(ticket);
        bags.remove(ticket);
        switch (ticket.getSizeType()){
            case BIG:
            {
                bigSlot++;
                break;
            }
            case MIDDLE:
            {
                middleSlot++;
                break;
            }
            case SMALL:
            {
                smallSlot++;
                break;
            }
        }
        return bag;
    }

}
