package com.twuc.webApp;

public class Bag {
    private SizeType size = SizeType.SMALL;

    public Bag(SizeType size) {
        this.size = size;
    }

    public Bag() {
    }

    public SizeType getSize() {
        return size;
    }
}
